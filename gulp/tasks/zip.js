var gulp   = require('gulp');
var zip   = require('gulp-zip');
var config = require('../config').zip;

/**
 * Gzip text files
 */
gulp.task('zip', function() {
    return gulp.src(config.src, {base: 'dist'})
        .pipe(zip('creative_package.zip'))
        .pipe(gulp.dest(config.dist));
});