'use strict';

var gulp = require('gulp');
var config = require('../config');
var assetConfig = require('../config').assetsConfig;
var assetDevConfig = require('../config').assetsDevConfig;
var handleErrors = require('../util/handleErrors');
var runSequence = require('run-sequence');


gulp.task('assets', function(cb) {
  //runSequence('assets:base', cb);
  runSequence('assets:base', 'assets:dev', cb);
});


gulp.task('assets:base', function() {
  //return gulp.src(config.src, { base: './src' })
  return gulp.src(assetConfig.src, { base: config.src })
    .pipe(gulp.dest(assetConfig.dest + '/'))
    .on('error', handleErrors);
});


gulp.task('assets:dev', function() {
  return gulp.src(assetDevConfig.src, { base: config.src })
    .pipe(gulp.dest(assetDevConfig.dest + '/'))
    .on('error', handleErrors);
});

