var gulp = require('gulp');
var config = require('../config');
var bannerConfig = config.bannerConfig;
var runSequence = require('run-sequence');



/**
 * Run all tasks needed for a build in defined order
 */
gulp.task('build', function(cb) {
  runSequence('delete', 'common', [
      //'images',
      'sprites',
      'sass',
      'scripts',
      'assets'
      // 'fonts',
      // 'markup',
      // 'vendor'
    ],
    //'base64',
    cb);
});
