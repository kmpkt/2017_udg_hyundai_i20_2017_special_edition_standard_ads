'use strict';

var gulp = require('gulp');
var config = require('../config');
var uglify = require('gulp-uglify');
var bannerConfig = config.bannerConfig;
var ignore = require('gulp-ignore');
var tap = require('gulp-tap');
var del = require('del');
var header = require('gulp-header');
var pkg = require('../../package.json');
var minifyCss = require('gulp-minify-css');
var runSequence = require('run-sequence');


gulp.task('delete:dist', function() {
  return del(
    config.dist
  );
});


gulp.task('js:dist', function() {
  return gulp.src(bannerConfig.map(function(banner) { return banner.js.dest + '/' + banner.js.outputName }), {base: './build'})
    .pipe(uglify())
    
    // .pipe(ignore.exclude(config.path.build + '/' + config.path.js + '/vendor/**'))
    // .pipe(header(banner, {
    //   pkg: pkg
    // }))
    .pipe(gulp.dest(config.dist));
});

gulp.task('assets:dist', function() {
  var a = bannerConfig.map(function(banner) { return banner.dest + '/**' });
  // Exclude build only test assets
  a = a.concat( bannerConfig.map(function(banner) { return '!' + banner.assetsDev.dest }) );
  a = a.concat( bannerConfig.map(function(banner) { return '!' + banner.assetsDev.dest + '/**'}) );
  return gulp.src(a, {base: './build'})
    .pipe(gulp.dest(config.dist));
});


gulp.task('html:dist', function() {
  return gulp.src(bannerConfig.map(function(banner) {  return banner.src+'/**/*.html' ; }), {base: './src'})
    .pipe(gulp.dest(config.dist));
});


// gulp.task('manifest:dist', function() {
//   return gulp.src(bannerConfig.map(function(banner) {  return banner.src+'/**/*.json' ; }), {base: './src'})
//     .pipe(gulp.dest(config.dist));
// });


gulp.task('sass:dist', function() {
  return gulp.src(bannerConfig.map(function(banner) { return banner.sass.dest+'/**' }), {base: './build'})
    .pipe(minifyCss({
          rebase: false,
          advanced: false,
          keepSpecialComments: 0
          // processImport:false,
          // compatibility: 'ie8'
        }))
    .pipe(gulp.dest(config.dist));
});

/**
 * Run all tasks needed for a build in defined order
 */
gulp.task('dist', function(cb) {
  runSequence('build', 'delete:dist', 'assets:dist', [
      'html:dist',
      // 'manifest:dist',
      'js:dist',
      'sass:dist'
      // 'scripts',
      // 'assets'
      // 'fonts',
      // 'markup',
      // 'vendor'
    ],
    //'base64',
    cb);
});
