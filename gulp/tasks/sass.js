'use strict';

var gulp = require('gulp');
var config = require('../config');
// Prevent pipe breaking caused by errors from gulp plugins
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var autoPrefixer = require('gulp-autoprefixer');
var handleErrors = require('../util/handleErrors');
var debug = require('gulp-debug');

gulp.task('sass', function() {
  console.log('Create css from scss.');
  return gulp.src(config.sass.src, { base: './src' })
    .pipe(debug({title: config.build}))
    // .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass(config.sass.options))
    .on('error', handleErrors)
    .pipe(autoPrefixer({
      browsers: config.autoprefixer.def,
      cascade: false
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(config.build+'/'));
});
