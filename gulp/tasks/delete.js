var gulp = require('gulp');
var del = require('del');
var configDev = require('../config').delete;
var configDist = require('../config').distribution;

/**
 * Delete folders and files
 */
gulp.task('delete', function() {
  return del(
    configDev.src
  );
});