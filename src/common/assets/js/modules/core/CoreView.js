'use strict';

import EventDispatcher from 'eventdispatcher';

export default class CoreView extends EventDispatcher {

  transitionInComplete = false;
  transitionOutComplete = false;

  constructor(el, props = {}) {
    super();
    if (!el) {
      throw new Error('Undefined el');
    } else {
      this.el = (typeof el === 'string') ? this._selector(el) : el;
      // this.el = (typeof(el) !== 'string') ? el : this._selector(el) || el;
    }
    this.props = props;
  }

  init() {
    this._setupDom();
    this.addListeners();
  }

  /****************************************
   *      PRIVATE / PROTECTED
   ****************************************/
  _selector(e) {
    let doc = window.document;
    return (typeof(doc) === 'undefined') ? e : (doc.querySelector ? doc.querySelector(e) : doc.getElementById((e.charAt(0) === "#") ? e.substr(1) : e));
  }

  _setupDom() {
  }

  /****************************************
   *      PUBLIC
   ****************************************/
  addListeners() {
  }

  removeListeners() {
  }

  transitionIn(dur = 1) {
    this.transitionInComplete = false;
    this.transitionOutComplete = false;
    let tl = new TimelineLite({onComplete: ::this.onTransitionInComplete});
    return tl;
  }

  transitionOut(dur = 1) {
    this.transitionInComplete = false;
    this.transitionOutComplete = false;
    let tl = new TimelineLite({onComplete: ::this.onTransitionOutComplete});
    return tl;
  }

  /****************************************
   *      EVENT-HANDLER
   ****************************************/
  onTransitionInComplete() {
    this.transitionInComplete = true;
  }

  onTransitionOutComplete() {
    this.transitionOutComplete = true;
  }

  /****************************************
   *      GETTER-SETTER
   ****************************************/
}

