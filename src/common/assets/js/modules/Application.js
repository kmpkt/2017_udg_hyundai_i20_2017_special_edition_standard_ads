'use strict';

import Content from './Content';

export default class Application {

  constructor(props) {
    this.props = (props || {});
    console.log(this.props);
  }

  init() {
    // Init application
    this._setupDom();
    this._addListener();
    this.show();
  }

  /****************************************
   *      PRIVATE / PROTECTED
   ****************************************/

  _setupDom() {
    this.mainEl = document.getElementById('creative');
    this.politeEl = document.getElementById('polite');

    this.content = new Content('#content-container', this.props);
  }

  _addListener() {
    this.mainEl.addEventListener('click', ::this.onBtnClick, false);
  }

  /****************************************
   *      PUBLIC
   ****************************************/
  show() {
    // Remove loader
    let spinner = document.getElementById('spinner');
    if (spinner) {
      spinner.parentNode.removeChild(spinner);
    }
    this.politeEl.style.visibility = 'visible';
    TweenLite.to(this.content.el, 1, { autoAlpha: 1, delay: 0.7, ease: Quad.easeOut });
    TweenLite.delayedCall(1.7, ::this.content.transitionIn);
    // this.content.transitionIn();
  }

  /****************************************
   *      EVENT-HANDLER
   ****************************************/
  onBtnClick(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    window.open(window.clickTag, '_blank');
  }

  /****************************************
   *      GETTER-SETTER
   ****************************************/
}