'use strict';

import Cta from './components/Cta';
import Logo from './components/Logo';
import View from './components/View';
import CoreView from './core/CoreView';

export default class Content extends CoreView {

  views = [];
  loopCounter = 0;
  loopComplete = false;

  constructor(el, props = {}) {
    super(el, props);
    this.init();
  }

  /****************************************
   *      PRIVATE / PROTECTED
   ****************************************/
  _setupDom() {
    this.viewsEl = this.el.querySelectorAll('.phase');

    let view, viewProps;
    for (let i = 0; i < this.viewsEl.length; i++) {
      view = new View('.phase-' + (i + 1));
      this.views.push(view);
    }

    this.logo = new Logo('.logo-container', {hasLogoColorChange: this.props.hasLogoColorChange});
    this.cta = new Cta('.cta-button', {maxWidth: this.props.ctaMaxWidth});

    this.coverContainerEl = this.el.querySelector('.cover-container');
    this.ccFirst = this.coverContainerEl.querySelector('.first');
    this.ccSecond = this.coverContainerEl.querySelector('.second');
    this.ccThird = this.coverContainerEl.querySelector('.third');
    let durCover1 = 0.7;
    let durCover2 = this.props.coverLayer.durCover2;
    let lineEase = Linear.easeNone;
    let scale1 = this.props.coverLayer.scale1;
    let scale2 = this.props.coverLayer.scale2;
    let scale3 = this.props.coverLayer.scale3;
    let coverTl1 = TweenLite.from(this.ccFirst, durCover1, {[scale1]: 0, transformOrigin: "0% 0%", ease: lineEase});
    let coverTl2 = TweenLite.from(this.ccSecond, durCover1 / 2, {[scale2]: 0, transformOrigin: "0% 0%", ease: lineEase});
    let coverTl3 = TweenLite.from(this.ccThird, durCover1, {[scale3]: 0, transformOrigin: this.props.coverLayer.coverTransformOrigin, ease: lineEase});
    this.coverTl = new TimelineMax({paused: true}).add(coverTl1).add(coverTl2).add(coverTl3);
    //
    let coverRevTl1 = TweenLite.to(this.ccThird, durCover2, {[scale3]: 0, transformOrigin: this.props.coverLayer.coverTransformOrigin, ease: lineEase});
    let coverRevTl2 = TweenLite.to(this.ccSecond, durCover2 / 2, {[scale2]: 0, transformOrigin: "0% 0%", ease: lineEase});
    let coverRevTl3 = TweenLite.to(this.ccFirst, durCover2, {[scale1]: 0, transformOrigin: "0% 0%", ease: lineEase});
    this.coverRevTl = new TimelineMax({paused: true}).add(coverRevTl1).add(coverRevTl2).add(coverRevTl3);
  }

  _loop() {
    this.loopCounter++;
    let maxLoops = this.props.loops;
    console.log(maxLoops, this.loopCounter);
    if (maxLoops !== -1 && this.loopCounter > maxLoops) {
      this.loopComplete = true;
      this.tl.stop();
    }
    // else {
    //   this.tl.restart();
    // }
  }

  /****************************************
   *      PUBLIC
   ****************************************/
  addListeners() {
    this.cta.addEventListener(Cta.ALIGN_COMPLETE, ::this.onCtaAlignComplete);
  }

  transitionIn() {
    this.tl = new TimelineLite({
      onComplete: function() {
        this.restart();
      }
    })
      .append(this.views[0].transitionIn(), '+=0.5')
      // .append(this.views[0].transitionTextOut(), '+=1.5')
      .append(this.coverTl.tweenFromTo(0, this.coverTl.duration(), {ease: Power3.easeOut}), '+=1.5')
      .append(this.logo.transitionIn(), '-=1.45')
      // .append(this.views[0].transitionIn(), '-=0.5')
      .append(this.cta.transitionIn(), '+=0.2')
      .call(::this._loop, [], this, '+=3')
      .append(this.coverRevTl.tweenFromTo(0, this.coverRevTl.duration(), {ease: Power2.easeInOut}), '-=0')
      .append(this.logo.transitionOut(), '-=1.2')
      .append(this.cta.transitionOut(), '-=1.5')
      .append(this.views[0].transitionOut(), '-=1.7');

    // Add debug scrubber if script is loaded
    if (creative.scrubber) {
      creative.scrubber({'timeline': this.tl});
    }
  }

  /****************************************
   *      EVENT-HANDLER
   ****************************************/
  onCtaAlignComplete() {
    if (this.props.centerCta) {
      let x = (this.props.centerCta - this.cta.getWidth()) / 2;
      this.cta.el.style.left = x + 'px';
    }
  }

  /****************************************
   *      GETTER-SETTER
   ****************************************/
}