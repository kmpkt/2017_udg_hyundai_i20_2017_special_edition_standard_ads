'use strict';

import CoreView from '../core/CoreView';

export default class View extends CoreView {

  constructor(el, props = {}) {
    super(el, props);
    this.init();
  }

  /****************************************
   *      PRIVATE / PROTECTED
   ****************************************/
  _setupDom() {
    this.backgroundEl = this.el.querySelector('.background');
    this.headlineEl = this.el.querySelector('h1');
    this.subHeadlineEl = this.el.querySelector('h2');
    console.log(this.backgroundEl);
  }

  /****************************************
   *      PUBLIC
   ****************************************/
  transitionIn() {
    let tl = super.transitionIn();
    tl.set(this.el, {autoAlpha: 1, display: 'block'});
    // .timeScale(this.props.timeScale)
    // if (this.backgroundEl) {
    //   tl.to(this.backgroundEl, 1, {autoAlpha: 1, ease: Quad.easeOut});
    // }
    if (this.headlineEl) {
      tl.to(this.headlineEl, 1, {autoAlpha: 1, ease: Quad.easeOut}, '-=0.1');
    }
    if (this.subHeadlineEl) {
      tl.to(this.subHeadlineEl, 1, {autoAlpha: 1, ease: Quad.easeOut}, '-=0.3');
    }
    return tl;
  }

  transitionOut() {
    let tl = super.transitionOut();
    if (this.headlineEl) {
      tl.to(this.headlineEl, 0.5, {autoAlpha: 0, ease: Quad.easeOut}, '-=0');
    }
    if (this.subHeadlineEl) {
      tl.to(this.subHeadlineEl, 0.5, {autoAlpha: 0, ease: Quad.easeOut}, '-=0.5');
    }
    return tl;
  }

  transitionTextOut(dur = 0.5) {
    let tl = new TimelineLite();
    tl.to(this.headlineEl, dur, {autoAlpha: 0, ease: Quad.easeOut});
    return tl;
  }

  /****************************************
   *      EVENT-HANDLER
   ****************************************/

  /****************************************
   *      GETTER-SETTER
   ****************************************/
}