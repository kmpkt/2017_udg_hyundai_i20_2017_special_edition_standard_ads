'use strict';

import CoreView from '../core/CoreView';
import width from 'dom-helpers/query/width';

export default class Cta extends CoreView {

  static ALIGN_COMPLETE = 'ctaAlignComplete';

  constructor(el, props = {}) {
    super(el, props);
    this.init();
  }

  /****************************************
   *      PRIVATE / PROTECTED
   ****************************************/
  _setupDom() {
    this.labelEl = this.el.querySelector('.label');
    setTimeout(::this._readjustPadding, 500)
  }

  _readjustPadding() {
    let labelWidth = window.getComputedStyle(this.labelEl, null).getPropertyValue('width').match(/\d+/);
    let widthDiff = Math.round(this.props.maxWidth - labelWidth);
    if (widthDiff <= 30 && widthDiff >= 0) {
      this.el.style.paddingLeft = this.el.style.paddingRight = (widthDiff / 2) + 'px';
    } else if (widthDiff < 0) {
      this.el.style.paddingLeft = this.el.style.paddingRight = 0;
    }
    this.dispatch(Cta.ALIGN_COMPLETE);
  }
  /****************************************
   *      PUBLIC
   ****************************************/
  transitionIn() {
    let tl = new TimelineLite();
    tl.to(this.el, 1, {autoAlpha: 1, ease: Quad.easeOut});
    return tl;
  }

  transitionOut() {
    let tl = new TimelineLite();
    tl.to(this.el, 0.5, {autoAlpha: 0, ease: Expo.easeOut});
    return tl;
  }

  /****************************************
   *      EVENT-HANDLER
   ****************************************/

  /****************************************
   *      GETTER-SETTER
   ****************************************/
  getWidth() {
    return width(this.el);
  }
}