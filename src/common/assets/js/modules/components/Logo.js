'use strict';

import CoreView from '../core/CoreView';

export default class Logo extends CoreView {

  constructor(el, props = {}) {
    super(el, props);
    this.init();
  }

  /****************************************
   *      PRIVATE / PROTECTED
   ****************************************/
  _setupDom() {
    this.logoWhite = this.el.querySelector('.white');
    this.logoBlue = this.el.querySelector('.blue');
    if (!this.props.hasLogoColorChange) {
      this.logoWhite.style.display = 'none';
      TweenLite.set(this.logoBlue, {autoAlpha: 1});
    }
  }

  /****************************************
   *      PUBLIC
   ****************************************/
  transitionIn() {
    let lt = super.transitionIn();
    if (this.props.hasLogoColorChange) {
      lt.to(this.logoWhite, 0.5, {autoAlpha: 0, ease: Expo.easeOut});
      lt.to(this.logoBlue, 0.5, {autoAlpha: 1, ease: Expo.easeOut}, '-=0.5');
    }
    return lt;
  }

  transitionOut() {
    let lt = super.transitionOut();
    if (this.props.hasLogoColorChange) {
      lt.to(this.logoWhite, 0.3, {autoAlpha: 1, ease: Expo.easeOut});
      lt.to(this.logoBlue, 0.3, {autoAlpha: 0, ease: Expo.easeOut}, '-=0.3');
    }
    return lt;
  }

  /****************************************
   *      EVENT-HANDLER
   ****************************************/

  /****************************************
   *      GETTER-SETTER
   ****************************************/
}