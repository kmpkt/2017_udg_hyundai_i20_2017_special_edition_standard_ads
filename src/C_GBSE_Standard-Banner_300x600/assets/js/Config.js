const config = {
  adId: 'c_gbse_standard-banner_300x600',
  adSize: {
    x: 0,
    y: 0,
    width: 300,
    height: 600
  },
  contentSize: {
    x: 0,
    y: 0,
    width: 300,
    height: 600
  },
  coverLayer: {
    durCover2: 0.9,
    scale1: 'scaleY',
    scale2: 'scaleX',
    scale3: 'scaleY',
    coverTransformOrigin: "100% 100%"
  },
  ctaMaxWidth: 200,
  hasLogoColorChange: true
};
export default config;
