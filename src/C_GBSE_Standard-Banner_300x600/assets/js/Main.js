'use strict';

import Config from './Config';
import Application from '../../../common/assets/js/modules/Application';
import ready from 'domready';

ready(() => {

  var creative = window.creative || {};
  // Merge markup js config attributes and Config.js attributes
  for (let attr in Config) {
    creative.config[attr] = Config[attr];
  }
  creative.app = new Application(creative.config);
  creative.app.init();
});



