const config = {
  adId: 'c_gbse_standard-banner_728x90',
  adSize: {
    x: 0,
    y: 0,
    width: 728,
    height: 90
  },
  contentSize: {
    x: 0,
    y: 0,
    width: 728,
    height: 90
  },
  coverLayer: {
    durCover2: 0.7,
    scale1: 'scaleY',
    scale2: 'scaleX',
    scale3: 'scaleY',
    coverTransformOrigin: "0% 100%"
  },
  ctaMaxWidth: 125,
  hasLogoColorChange: false
};
export default config;
