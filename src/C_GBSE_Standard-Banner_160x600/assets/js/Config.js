const config = {
  adId: 'c_gbse_standard-banner_160x600',
  adSize: {
    x: 0,
    y: 0,
    width: 160,
    height: 600
  },
  contentSize: {
    x: 0,
    y: 0,
    width: 160,
    height: 600
  },
  coverLayer: {
    durCover2: 0.9,
    scale1: 'scaleX',
    scale2: 'scaleY',
    scale3: 'scaleX',
    coverTransformOrigin: "100% 100%"
  },
  ctaMaxWidth: 135,
  centerCta: 145,
  hasLogoColorChange: true
};
export default config;
