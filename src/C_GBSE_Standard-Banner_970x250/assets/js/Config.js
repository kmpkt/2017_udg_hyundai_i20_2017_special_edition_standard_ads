const config = {
  adId: 'c_gbse_standard-banner_970x250',
  adSize: {
    x: 0,
    y: 0,
    width: 970,
    height: 250
  },
  contentSize: {
    x: 0,
    y: 0,
    width: 970,
    height: 250
  },
  coverLayer: {
    durCover2: 0.9,
    scale1: 'scaleY',
    scale2: 'scaleX',
    scale3: 'scaleY',
    coverTransformOrigin: "0% 100%"
  },
  ctaMaxWidth: 175,
  hasLogoColorChange: false
};
export default config;
