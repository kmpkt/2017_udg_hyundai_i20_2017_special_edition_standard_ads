# Documentation Hyundai HME HTML5 Creatives



## Basics

This project is based on the Node.js package manager, formerly known as  **npm**, (https://www.npmjs.com/) and **Gulp** (gulpjs, http://gulpjs.com/) as Taskrunner to develop and distribute the Hyundai HME HTML5 Ads.



## How to start
Make sure that **npm**, and **gulp**, is properly installed and running on your system. Open up your terminal, navigate to your project folder and enter: 

```
npm install
```
Now all node packages and dependencies will be installed. This process may take a couple of minutes.
After finishing this process your project folder should contain a folder called **node_modules**.
Further informations: https://docs.npmjs.com/


## Compile for development
To start the **Gulp** development tasks, enter following command in your console of your project folder:

```
npm run dest
```
The ads will be compiled to **/build**. All necessary assets from common and the individuell ad folders will be copied to this folder, too.
Further informations:  http://gulpjs.com/

## Compile for distribution
To start the **Gulp** for distribution tasks, enter following command in your console of your project folder:

```
npm run dist
```
The ads will be minified, uglyfied and compiled to **/dist**. All necessary assets from common and the individuell ad folders will be copied to this folder, too.


## Structure
Relevant files to customize the banners:

	|-- build
	|-- dist
	|-- src
    	|-- common
		    |-- assets
	    		|-- css
	    		|-- fonts
			    |-- images
			    |-- js
	    |-- wallpaper_160x600
		    |-- assets
			    |-- css
			    |-- images
			    |-- js
			    index.html

    

**build**

This is the target folder for the development output.

**dist**

This is the target folder for the compiled and minified banner.

**src**

This is the banner source folder for all kind of assets: 

*js, css, images and fonts*

There are two different kind of folders: 
1. The **/common** package contains all base and shared assets. All these assets will be used by **EVERY** ad. Therefore, every code or file change will effect every ad! 
2. The ad source folders (e.g. **/wallpaper_160x600**)  contain all individuell assets und code for each single format. 


## Customization

###css (sass)###
The css is written in sass (http://sass-lang.com/)
The main code can get found in  **_creative.scss**, **_format.scss** and **_settings.scss** files (common and individual project folder).
Keep in mind, editing files in the common package will effect all ads!

###js###
The main code of each banner can be found **CA_AE_.../assets/js/Application.js**. 



## Add new ad format
If you want to add a new ad to your project you have to edit two files in the gulp structure folder.

		|-- gulp

Add the name of the ad to the an array in the config.js file.
gulp/config.js.
var bannerFolder = ['BannerName1', 'BannerName2' ];
